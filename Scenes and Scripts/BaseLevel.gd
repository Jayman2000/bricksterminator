# Bricksterminator - Example ball and paddle game for helping people learn Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node


const BALL = preload("res://Scenes and Scripts/Ball.tscn")

export(int) var lives = 3
export(PackedScene) var next_level = load("res://Scenes and Scripts/BaseLevel.tscn")

onready var score = 0


func _ready():
	get_tree().call_group("breakable", "connect", "give_points", self, "add_to_score")
	get_tree().call_group("breakable", "connect", "die", self, "possibly_change_level")
	spawn_ball()


func _on_DeathZone_body_entered(body):
	body.queue_free()

	lives -= 1

	if lives >= 0:
		$LifeCounter.text = "Lives: " + str(lives)
		spawn_ball()


func add_to_score(points_to_add):
	score += points_to_add
	$ScoreCounter.text = "Score: " + str(score)

# Check to see if all of the bricks have been cleared. If they have been cleared
# switch to the next level.
func possibly_change_level():
	var tree = get_tree()
	var breakables_left = tree.get_nodes_in_group("breakable")
	if len(breakables_left) == 0:
		tree.change_scene_to(next_level)

func spawn_ball():
	var to_add = BALL.instance()
	to_add.position = $BallSpawn.position
	call_deferred("add_child", to_add)
