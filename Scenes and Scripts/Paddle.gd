# Bricksterminator - Example ball and paddle game for helping people learn Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

tool
extends Area2D


# If the Ball hits the Paddle's left corner, its new angle will be -MAX_ANGLE.
# If the Ball hits the Paddle's center, its new angle will be 0.
# If the Ball hits the Paddle's right corner, its new angle will be MAX_ANGLE.
#
# If the Ball hits the Paddle between its left corner and its center, its new
# angle will be between -MAX_ANGLE and 0.
# If the Ball hits the Paddle between its center and its right corner, its new
# angle will be between 0 and MAX_ANGLE.
const MAX_ANGLE = deg2rad(85) # Unit: radians
# How long each of the Paddle's red caps are before scaling. In other words,
# this is how long, in pixels, "res://Paddle End.png" is.
const PRESCALE_END_LENGTH = 70
# How much faster the Ball will go after bouncing off the Paddle.
const SPEED_MULTIPLIER = 1.05

export(float, 0, 800) var length = 100 setget set_length # Unit: pixels
export(float, 1, 800) var speed = 250 # Unity: pixels per second

# If true, allow the player to move using the mouse.
# TODO: Put this in a singleton so that mouse controls will stay on between
# level transitions.
onready var mouse_controls = true


func _physics_process(delta):
	if not Engine.editor_hint:
		var velocity = 0
		if Input.is_action_pressed("ui_left"):
			mouse_controls = false
			velocity -= Input.get_action_strength("ui_left") * speed * delta
		if Input.is_action_pressed("ui_right"):
			mouse_controls = false
			velocity += Input.get_action_strength("ui_right") * speed * delta
		set_position_x(position.x + velocity)

func _ready():
	if not Engine.editor_hint and mouse_controls:
		# When using mouse controls, the Paddle gets moved when the mouse is
		# moved. When a level starts, the player will not necessarily be moving
		# their mouse. This means that the Paddle could start away from the
		# mouse. To prevent this issue from happening, the following causes the
		# Paddle to move to where the mouse is.
		set_position_x(get_viewport().get_mouse_position().x)

func _unhandled_input(event):
	if not Engine.editor_hint:
		match event.get_class():
			"InputEventMouseButton":
				mouse_controls = true
			"InputEventMouseMotion":
				if mouse_controls:
					set_position_x(event.position.x)


func _on_Paddle_body_entered(body):
	if not Engine.editor_hint:
		# Make the ball bounce off of the Paddle

		# A distance_ratio of -1 means the Ball hit the Paddle's left corner.
		# A distance_ratio of  0 means the Ball hit the Paddle's center.
		# A distance_ratio of  1 means the Ball hit the Paddle's right corner.
		#
		# A distance_ratio between -1 and 0 means the Ball hit the Paddle
		# between its left corner and its center.
		# A distance_ratio between 0 and 1 means the Ball hit the Paddle
		# between its center and its right corner.
		var distance_ratio = (body.position.x - position.x) / (length / 2)
		distance_ratio = clamp(distance_ratio, -1, 1)

		var new_velocity = Vector2.UP
		new_velocity = new_velocity.rotated(MAX_ANGLE * distance_ratio)
		new_velocity *= body.linear_velocity.length() * SPEED_MULTIPLIER

		body.linear_velocity = new_velocity


func set_length(new_length):
	# The length of either of the Paddle's ends after scaling has been applied.
	var end_length = PRESCALE_END_LENGTH * scale.x # Unit: pixels

	# Make sure that the Paddle is long enough to let both of the ends fit on
	# it.
	if new_length < end_length * 2:
		new_length = end_length * 2

	# The length of the Paddle's Middle before scaling has been applied.
	var prescale_middle_length = new_length / scale.x - 2 * PRESCALE_END_LENGTH

	# When the game is first run, this function gets run before this Node's
	# children are added. The following if statement makes Godot wait until the
	# children are added.
	if not is_inside_tree():
		yield(self, "ready")
	# Thanks, DleanJeans
	# https://github.com/godotengine/godot/issues/30460#issuecomment-569478670

	length = new_length

	# Put the Sprites in the right positions
	$Middle.scale.x = prescale_middle_length
	$Left.position.x = -(prescale_middle_length/2)
	$Right.position.x = prescale_middle_length/2

	# Resize the collsion shape
	$CollisionShape2D.shape.height = prescale_middle_length

func set_position_x(new_x):
	var viewport_rect = get_viewport_rect()
	var left_edge = viewport_rect.position.x
	var right_edge = viewport_rect.end.x

	position.x = clamp(new_x, left_edge + length/2, right_edge - length/2)
