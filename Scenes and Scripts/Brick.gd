# Bricksterminator - Example ball and paddle game for helping people learn Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends StaticBody2D


signal give_points(points)
signal die # Always remove Brick from the breakable group before emitting die


func hit():
	emit_signal("give_points", 50)
	remove_from_group("breakable")
	emit_signal("die")
	queue_free()
