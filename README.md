# Bricksterminator
#### Copying
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/Jayman2000">
    <span property="dct:title">Jason Yundt</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Bricksterminator</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="https://gitlab.com/Jayman2000">
  United States</span>.
</p>

Some parts of this game were made by other people or weren't dedicated to the public domain using CC0. Here's a list of stuff that wasn't created by me and where it appears in this repository:
  * ["A collection of .gitignore templates"](https://github.com/github/gitignore) by [GitHub](https://github.com), [CC0 1.0](https://github.com/github/gitignore/blob/master/LICENSE).
    * .gitignore
  * [CC0 software notice](https://wiki.creativecommons.org/wiki/CC0_FAQ#May_I_apply_CC0_to_computer_software.3F_If_so.2C_is_there_a_recommended_implementation.3F) by [Diane Peters](https://wiki.creativecommons.org/wiki/User:CCID-diane_peters), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
    * A modified version of this notice is at the top of every souce code file.
  * [A code snipet](https://github.com/godotengine/godot/issues/30460#issuecomment-569478670) by [DleanJeans](https://github.com/DleanJeans).
    * Scenes and Scripts/Paddle.gd
  * [HTML CC0 waiver](https://creativecommons.org/choose/zero/results?license-class=zero&name=Jason+Yundt&actor_href=https%3A%2F%2Fgitlab.com%2FJayman2000&work_title=Bricksterminator&work_jurisdiction=US&confirm=confirm&understand=confirm&lang=en_US&field1=continue&waiver-affirm=affirm) by [Creative Commons](https://creativecommons.org), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).
    * This is the HTML placed at the top of the "Copying" section of README.md
  * ["Kenney Fonts"](https://kenney.nl/assets/kenney-fonts) by [Kenney](https://kenney.nl), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0).
    * Art/Kenney Future Narrow.ttf
  * ["Puzzle Pack 2"](https://kenney.nl/assets/puzzle-pack-2) by [Kenney](https://kenney.nl), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0).
    * Art/Ball.png
    * Art/Paddle End.png
    * Art/Paddle Middle.png
  * [The CC0 1.0 Universal legal code as plaintext](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt) by [Creative Commons](https://creativecommons.org), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0).
    * COPYING.txt
